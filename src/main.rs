#![warn(missing_docs)]

use clap::{Parser, Subcommand};

#[derive(Subcommand, Debug)]
enum Commands {
    #[clap(about = "Start the program")]
    Start {
        #[clap(default_value_t=String::from("/etc/mem-db/config.toml"))]
        config_path: String,
    }
}

#[derive(Parser)]
#[clap(version = "0.1.0", about = "mem-db", long_about = None)]
#[clap(arg_required_else_help = true)]
#[clap(name = "mem-db")]
struct CLI {
    #[clap(subcommand)]
    command: Option<Commands>,
}


#[tokio::main]
async fn main() {
    let cli = CLI::parse();

    match cli.command {
        Some(Commands::Start { config_path }) => {
            println!("{}", config_path);
        },
        _ => {}
    };
}
